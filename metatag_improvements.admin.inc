<?php
function metatag_improvements_config_overview() {
  module_load_include('inc', 'metatag', 'metatag.admin');
  $arrbody = array();

  $arrbody = array_merge($arrbody, drupal_get_form('metatag_improvements_admin_form'));
  if(function_exists('metatag_config_overview')) {
    $arrbody = array_merge($arrbody, metatag_config_overview());
  }

  return $arrbody;
}
function metatag_improvements_admin_form() {
  $types = node_type_get_names();
  $form['metatag_improvements_allowed_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display meta tags for these content types'),
    '#default_value' => variable_get('metatag_improvements_allowed_types', array_keys($types)),
    '#options' => $types,
  );
  $form['metatag_improvements_title_maxlength'] = array(
    '#type' => 'textfield',
    '#title' => t('Max length for title tag'),
    '#default_value' => variable_get('metatag_improvements_title_maxlength', 65),
  );
  $form['metatag_improvements_description_maxlength'] = array(
    '#type' => 'textfield',
    '#title' => t('Max length for description tag'),
    '#default_value' => variable_get('metatag_improvements_description_maxlength', 155),
  );
  return system_settings_form($form);
}