<?php

class views_metatags_plugin_display_extender_metatags extends views_plugin_display_extender {

  private function has_metatags() {
    return views_metatags_views_display_has_metatags($this->display);
  }

  function option_definition(&$options) {
    $options['metatags'] = array('default' => array());
  }

  function options_summary(&$categories, &$options) {
    if ($this->has_metatags()) {
      $categories['metatags'] = array(
        'title' => t('Meta tags'),
        'column' => 'second',
      );
      $metatags = $this->display->get_option('metatags');
      $options['metatags'] = array(
        'category' => 'metatags',
        'title' => t('Meta tags'),
        'value' => !empty($metatags) ? t('Overridden') : t('Using defaults'),
      );
    }
  }

  function options_form(&$form, &$form_state) {
    if ($form_state['section'] == 'metatags') {
      $form['#title'] .= t('The meta tags for this display');
      $instance = 'views:' . $form_state['view']->name;
      $metatags = $this->display->get_option('metatags');
      $options['token types'] = array('view');
      metatag_metatags_form($form, $instance, $metatags, $options);
      $form['metatags']['#type'] = 'container';
    }
  }

  function options_submit(&$form, &$form_state) {
    if ($form_state['section'] == 'metatags') {
      $metatags = $form_state['values']['metatags'];
      metatag_filter_values_from_defaults($metatags);
      $this->display->set_option('metatags', $metatags);
    }
  }

  function pre_execute() {
    //dpm($this->display);
    //$metatags = $this->display->get_option('metatags');
    //dpm($metatags);
    if ($metatags = $this->display->get_option('metatags')) {
      $instance = $this->display->view->name . ':' . $this->display->view->current_display;
  
       // Build options for meta tag rendering.
      $options = array();
      $options['token data']['view'] = $this->display->view;
    
      // Render the metatags.
      $output = array_intersect_key(metatag_metatags_view($instance,$metatags, $options),$metatags);
      render($output);
    }
  }

  function query() {
  }
}
