<?php

/**
 * @file
 * Views integration for the metatag module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_metatags_views_plugins() {
  $plugins['display_extender']['metatags'] = array(
    'title' => t('Meta tags'),
    'help' => t('Provides meta tags for views.'),
    'handler' => 'views_metatags_plugin_display_extender_metatags',
  );

  return $plugins;
}
