<?php

/**
 * @file
 * Provides native meta tag integration with Views.
 */

/**
 * Implements hook_views_api().
 */
function views_metatags_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Implements hook_ctools_plugin_api().
 */
function views_metatags_ctools_plugin_api($owner, $api) {
  if ($owner == 'metatag' && $api == 'metatag') {
    return array('version' => 1);
  }
}

function views_metatags_views_display_has_metatags(views_plugin_display $display) {
  if (method_exists($display, 'has_metatags')) {
    return $display->has_metatags();
  }
  else {
    return $display->has_path() && $display->uses_breadcrumb();
  }
}

/**
 * Implements hook_view_preview_info_alter().
 */

function views_metatags_views_preview_info_alter(&$rows, $view) {
  if (views_metatags_views_display_has_metatags($view->display_handler)) {
    $instance = 'views:' . $view->name;
    $metatags = $view->display_handler->get_option('metatags');

    // Set the page title to be the previewed views title before fetching
    // meta tag values.
    $title = drupal_set_title();
    if ($view_title = $view->get_title()) {
      drupal_set_title($view_title);
    }

    $options['token data']['view'] = $view;
    $values = metatag_metatags_values($instance, $metatags, $options);
    foreach ($values as $metatag => $value) {
      $metatag_info = metatag_get_info('tags', $metatag);
      $values[$metatag] = check_plain($metatag_info['label']) . ': ' . check_plain($value);
    }
    $rows['query'][] = array(
      '<strong>' . t('Meta tags') . '</strong>',
      implode('<br />', $values),
    );

    // Restore the page title.
    drupal_set_title($title);
  }
}
/**
 * Implements hook_page_alter().
 */
function views_metatags_page_alter(&$page) {
  $view = views_get_page_view();
  if (!path_is_admin(current_path()) && !empty($view)) {
    // If a module is still putting in the display like we used to, catch that.
    if (is_subclass_of($view, 'views_plugin_display')) {
      $view = $view->view;
    }

    if ($metatags = $view->display_handler->get_option('metatags')) {
      $instance = $view->display_handler->view->name . ':' . $view->display_handler->view->current_display;

      // Build options for meta tag rendering.
      $options = array();
      $options['token data']['view'] = $view->display_handler->view;

      // Add the metatags.
      $page['content']['metatags'][$view->display_handler->view->name] = metatag_metatags_view($instance, $metatags, $options);
    }
  }
}
